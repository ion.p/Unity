﻿var LookAtTarget:Transform;
var bullitPrefab:Transform;    
var damp=6.0;
var seconds;
var savedTime;

function Update(){
	if(LookAtTarget){
		var rotate = Quaternion.LookRotation(LookAtTarget.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation,rotate, Time.deltaTime*damp);
		var seconds : int = Time.time;
		var addeven = (seconds%2);
		
		if(addeven){
			shoot(seconds);	
		}			
	}
}

function shoot(seconds){
    if(seconds!=savedTime){
  		var bullit = Instantiate(bullitPrefab,
                    transform.Find("spawnPoint").transform.position,
                    Quaternion.identity); 		
        bullit.gameObject.tag = "enemyProtectile";
        
        bullit.GetComponent.<Rigidbody>().AddForce(transform.forward * 1000);        
	    savedTime = seconds;
    }
}

