﻿///moving around
var speed = 3.0;
var rotateSpeed = 3.0;
//Shooting
var bullitPrefab : Transform;  
//dying
static var dead = false;
//geting hit 
var tumbleSpeed = 800;
var decreaseTime = 0.01;
var decayTime = 0.01;
static var gotHit = false;
private var backup = [tumbleSpeed,decreaseTime,decayTime];

function LateUpdate(){
   	if(dead){
   		transform.position=Vector3(0,4,0);
   		gameObject.Find("Main Camera").transform.position=Vector3(0,4,-10);
   		dead=false;
   	}
   	if(gotHit){
   		if(tumbleSpeed<1){
   			//reset and get bakc in the game
   			tumbleSpeed = backup[0];
   			decreaseTime = backup[1];
   			decayTime = backup[2];
   			gotHit = false;
   		}
   		else{
   			//spin around after hitted
   			transform.Rotate(0,tumbleSpeed*Time.deltaTime,0,Space.World);
   			tumbleSpeed = tumbleSpeed-decreaseTime;
   			decreaseTime += decayTime;
   		}  	
   	}
}
//function OnControllerColliderHit(hit:ControllerColliderHit)
function OnTriggerEnter(hit : Collider)
{
	
	if(hit.gameObject.tag=="fallout"){
	
		dead=true;
		//substract life here
		HealthControl.LIVES -=1;
	}
	if(hit.gameObject.tag=="enemyProtectile"){
		gotHit = true;
		HealthControl.HITS +=1;
		Destroy(hit.gameObject);
	}
} 
      
function Update () 
    {
    var controller : CharacterController = GetComponent(CharacterController);
    
    // Rotate around y - axis
    transform.Rotate(0, Input.GetAxis ("Horizontal") * rotateSpeed, 0);
    
    // Move forward / backward
    var forward = transform.TransformDirection(Vector3.forward);
    var curSpeed = speed * Input.GetAxis ("Vertical");
    controller.SimpleMove(forward * curSpeed);
        
 	if(Input.GetButtonDown("Jump"))        
 	{
  		var bullit = Instantiate(bullitPrefab,
                    transform.Find("spawnPoint").transform.position,
                    Quaternion.identity); 		
        
        bullit.tag = "wormProtectile";
        
        bullit.GetComponent.<Rigidbody>().AddForce(transform.forward * 2020);        
        }
    }

   @script RequireComponent(CharacterController)   