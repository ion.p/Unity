﻿var health1 : Texture2D; //one live left
var health2 : Texture2D; //two lives left
var health3 : Texture2D; //three lives left
var worm_bodyPart1 : Transform;
var worm_bodyPart2 : Transform;

static var LIVES = 3;
static var HITS = 0;

function Update () {
	print("Lives: "+LIVES+" Hits: "+HITS);//lives 3 hits 0

	switch(LIVES){
		case 3:
			GetComponent.<GUITexture>().texture = health3;
		break;
		case 2:
			GetComponent.<GUITexture>().texture = health2;
		break;
		case 1:
			GetComponent.<GUITexture>().texture = health1;
		break;
		
		case 0:
		//game over
		break;
	}
	switch(HITS){
		case 1:
			worm_bodyPart2.GetComponent.<Renderer>().enabled=false;			
		break;
		case 2:
			worm_bodyPart1.GetComponent.<Renderer>().enabled=false;
			worm_bodyPart2.GetComponent.<Renderer>().enabled=false;	
		break;
		case 3:
			LIVES -= 1;
			HITS = 0;
			MoveAround.dead = true;
			worm_bodyPart1.GetComponent.<Renderer>().enabled=true;
			worm_bodyPart2.GetComponent.<Renderer>().enabled=true;

			break;
	}	
}